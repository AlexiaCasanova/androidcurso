package com.example.recyclerviewejemplo
import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter : RecyclerView.Adapter<ViewHolder>() {
    var contactos: MutableList<Contacto> = ArrayList()
    lateinit var context:Context

    fun RecyclerAdapter(contactos : MutableList<Contacto>, context: Context){
        this.contactos = contactos
        this.context = context
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val item = contactos.get(position)
        holder.bind(item, context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder{
        val layoutInflater= layoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.recyclerView, parent, false))

    }
    override fun getItemCount() : Int {
        return contactos.size
    }
}