package com.example.recyclerviewejemplo

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_contact.view.*

class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
    val name = view.findViewById(R.id.name) as TextView

    fun blind(contacto:Contacto, context: Context){
        name.text = contacto.name
        phone.text = contacto.phone
        itemView.setOnClickListener(View.onClickListener{
            Toast.makeText(context , contacto.name, Toast.LENGTH_SHORT)
        })
    }


}