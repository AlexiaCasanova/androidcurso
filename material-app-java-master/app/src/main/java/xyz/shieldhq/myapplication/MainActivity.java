package xyz.shieldhq.myapplication;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.app_welcome);

        final TextView center_label = findViewById(R.id.center_label);



        FloatingActionButton fab_a = findViewById(R.id.fab_a), fab_b = findViewById(R.id.fab_b), fab_c = findViewById(R.id.fab_c), fab_d = findViewById(R.id.fab_d), fab_e = findViewById(R.id.fab_e);

        fab_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.fab_a_snack, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        fab_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, R.string.fab_b_toast, Toast.LENGTH_SHORT).show();
            }
        });


        fab_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.fab_c_snack, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        fab_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, R.string.fab_d_snack, Toast.LENGTH_SHORT).show();
            }
        });


        fab_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                center_label.setText(R.string.name);
                Snackbar.make(view, R.string.fab_e_snack, Snackbar.LENGTH_LONG)
                .setAction(R.string.fab_e_snack_action, new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        center_label.setText(R.string.hello_world);
                    }
                }).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        switch (id) {

            case R.id.action_settings: {

                Toast.makeText(this, R.string.toast_main_menu_settings, Toast.LENGTH_SHORT).show();
                return true;
            }

            case R.id.action_facebook: {

                Toast.makeText(this, R.string.toast_main_menu_facebook, Toast.LENGTH_SHORT).show();
                return true;
            }

            case R.id.action_nextcloud: {

                Toast.makeText(this, R.string.toast_main_menu_nextcloud, Toast.LENGTH_SHORT).show();
                return true;
            }

            case R.id.action_logout: {

                Toast.makeText(this, R.string.toast_main_menu_logout, Toast.LENGTH_SHORT).show();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
