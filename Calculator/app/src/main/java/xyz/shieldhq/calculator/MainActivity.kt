package xyz.shieldhq.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        updateDisplay("")
    }

    val operationList: MutableList<String> = arrayListOf()
    val numberCache: MutableList<String> = arrayListOf()


    //I couldn't find mkString.. so I improvised
    fun makeString(list: List<String>,joiner: String = "") : String {

        if (list.isEmpty()) return ""
        return list.reduce { r, s -> r + joiner + s }
    }

    private fun clearCache(view: View){

        numberCache.clear()
        operationList.clear()
    }
    //OnClick
    fun clearClick(view: View){
        clearCache()
        updateDisplay("")
    }
    fun clearCache(){
        numberCache.clear()
        operationList.clear()
    }

    fun equalsClick(view: View){
        operationList.add(makeString(numberCache))
        numberCache.clear()

        val calculator = StringCalculator()
        val answer = calculator.calculate(operationList)

        updateDisplay("=" + answer.toString())
        clearCache(view)

    }

    fun buttonClick(view: View){

        val button = view as Button
        if(numberCache.isEmpty()) return

        operationList.add(makeString(numberCache))
        numberCache.clear()
        operationList.add(button.text.toString())

        updateDisplay(button.text.toString())

    }
    fun numberClick(view: View){
        val button = view as Button
        val numberString = button.text

        numberCache.add(numberString.toString())
        val text = makeString(numberCache);
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        updateDisplay(text)
    }
    fun updateDisplay(string: String){
        var fullCalculationString = makeString(operationList, "")
        var fullCalculationTextView = findViewById(R.id.fullCalculationText) as TextView
        fullCalculationText.text = fullCalculationString

        val mainTextView = findViewById(R.id.textView) as TextView
        mainTextView.text = string

    }


}
