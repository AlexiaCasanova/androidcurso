package com.example.retrofit_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.retrofit_app.R;
import com.example.retrofit_app.adapter.customAdapter;
import com.example.retrofit_app.model.RetroPhoto;
import com.example.retrofit_app.network.GetDataService;
import com.example.retrofit_app.network.RetroFitinstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity {
    private customAdapter CustomAdapter;
    private RecyclerView recyclerView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();

        GetDataService service = RetroFitinstance.getRetrofitInstance().create(GetDataService.class);

        Call<List<RetroPhoto>> call = service.getAllPhotos();
        call.enqueue(new Callback<List<RetroPhoto>>() {
            @Override
            public void onResponse(Call<List<RetroPhoto>> call, Response<List<RetroPhoto>> response) {
                progressDialog.dismiss();
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<RetroPhoto>> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Fallo la conexion",Toast.LENGTH_SHORT).show();
            }
        });

        private void generateDataList(List<RetroPhoto> photoList){
            recyclerView = findViewById(R.id.customRecyclerView);
            CustomAdapter = new customAdapter(this, photoList);
            RecyclerView.LayoutManager  layoutManager = new LinearLayoutManager(MainActivity.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(CustomAdapter);

        }

    }
}
